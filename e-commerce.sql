/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 100424
 Source Host           : localhost:3306
 Source Schema         : e-commerce

 Target Server Type    : MySQL
 Target Server Version : 100424
 File Encoding         : 65001

 Date: 10/03/2023 16:41:49
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for bills
-- ----------------------------
DROP TABLE IF EXISTS `bills`;
CREATE TABLE `bills`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `orders_id` int(11) NOT NULL,
  `products_id` int(11) NOT NULL,
  `bill_no` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `created_by` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  `updated_by` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `deleted_at` datetime(0) NULL DEFAULT NULL,
  `deleted_by` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `fk_bills_products1_idx`(`products_id`) USING BTREE,
  INDEX `fk_bills_orders1_idx`(`orders_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of bills
-- ----------------------------
INSERT INTO `bills` VALUES (8, 22, 1, 'bill-1678440916', '2023-03-10 16:35:16', '', '2023-03-10 16:35:16', '', NULL, '');
INSERT INTO `bills` VALUES (9, 22, 2, 'bill-1678440916', '2023-03-10 16:35:16', '', '2023-03-10 16:35:16', '', NULL, '');
INSERT INTO `bills` VALUES (10, 22, 3, 'bill-1678440916', '2023-03-10 16:35:16', '', '2023-03-10 16:35:16', '', NULL, '');
INSERT INTO `bills` VALUES (11, 23, 1, 'bill-1678441166', '2023-03-10 16:39:26', '', '2023-03-10 16:39:26', '', NULL, '');

-- ----------------------------
-- Table structure for login
-- ----------------------------
DROP TABLE IF EXISTS `login`;
CREATE TABLE `login`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ip` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `message` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `created_by` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of login
-- ----------------------------
INSERT INTO `login` VALUES (1, 'apiwat', '', '200', 'success', '2023-03-03 09:22:55', 'apiwat');
INSERT INTO `login` VALUES (2, 'apiwat', '', '200', 'success', '2023-03-03 09:23:30', 'apiwat');
INSERT INTO `login` VALUES (3, 'apiwat', '', '200', 'success', '2023-03-10 00:00:45', 'apiwat');
INSERT INTO `login` VALUES (4, 'apiwat', '', '200', 'success', '2023-03-10 00:02:25', 'apiwat');
INSERT INTO `login` VALUES (5, 'apiwat', '', '200', 'success', '2023-03-10 00:05:37', 'apiwat');
INSERT INTO `login` VALUES (6, 'apiwat', '', '200', 'success', '2023-03-10 00:06:30', 'apiwat');
INSERT INTO `login` VALUES (7, 'apiwat', '', '200', 'success', '2023-03-10 01:04:08', 'apiwat');
INSERT INTO `login` VALUES (8, 'apiwat', '', '200', 'success', '2023-03-10 01:05:58', 'apiwat');
INSERT INTO `login` VALUES (9, 'apiwat', '', '200', 'success', '2023-03-10 01:07:34', 'apiwat');
INSERT INTO `login` VALUES (10, 'apiwat', '', '200', 'success', '2023-03-10 01:10:03', 'apiwat');
INSERT INTO `login` VALUES (11, 'apiwat', '', '200', 'success', '2023-03-10 14:25:45', 'apiwat');
INSERT INTO `login` VALUES (12, 'apiwat', '', '200', 'success', '2023-03-10 15:27:24', 'apiwat');
INSERT INTO `login` VALUES (13, 'apiwat', '', '200', 'success', '2023-03-10 16:17:21', 'apiwat');

-- ----------------------------
-- Table structure for orders
-- ----------------------------
DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `users_id` int(11) NOT NULL,
  `status` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `slip_path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `approved` enum('Y','N') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'N',
  `created_at` datetime(0) NULL DEFAULT NULL,
  `created_by` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  `updated_by` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `deleted_at` datetime(0) NULL DEFAULT NULL,
  `deleted_by` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `total_price` float NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `fk_order_histories_users_idx`(`users_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 24 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of orders
-- ----------------------------
INSERT INTO `orders` VALUES (22, 1, 'waiting', '', '', '2023-03-10 16:35:16', '', '2023-03-10 16:35:16', '', NULL, '', 0);
INSERT INTO `orders` VALUES (23, 1, 'cancel', '', '', '2023-03-10 16:39:26', '', '2023-03-10 16:40:34', '', NULL, '', 0);

-- ----------------------------
-- Table structure for products
-- ----------------------------
DROP TABLE IF EXISTS `products`;
CREATE TABLE `products`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `price` float NULL DEFAULT NULL,
  `path_image` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `product_amount` int(11) NULL DEFAULT NULL,
  `status` enum('Y','N') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'Y',
  `created_at` datetime(0) NULL DEFAULT NULL,
  `created_by` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  `updated_by` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `deleted_at` datetime(0) NULL DEFAULT NULL,
  `deleted_by` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of products
-- ----------------------------
INSERT INTO `products` VALUES (1, 'ปากกา', 10, NULL, 300, 'Y', '2023-03-10 14:31:03', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `products` VALUES (2, 'ดินสอ', 5, NULL, 300, 'Y', '2023-03-10 14:31:16', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `products` VALUES (3, 'ยางลบ', 8, NULL, 150, 'Y', '2023-03-10 14:31:31', NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `first_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `last_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `address` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status` enum('Y','N') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'Y',
  `created_at` datetime(0) NULL DEFAULT NULL,
  `created_by` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  `updated_by` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `deleted_at` datetime(0) NULL DEFAULT NULL,
  `deleted_by` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, 'apiwat', '$2a$14$DUaJIg0akvhj74GwSRZYzeix7F5EDEyeuGZOqP2JUicqWQwVgLeWG', 'apiwat', 'mekwan', '123/1 m.1 sila muang khonkaen', '', '2023-03-03 09:19:07', 'apiwat', '2023-03-03 09:19:07', '', NULL, '');

SET FOREIGN_KEY_CHECKS = 1;
