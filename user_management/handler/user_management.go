package handler

import (
	"github.com/golang-jwt/jwt"
	"github.com/labstack/echo/v4"
	"net/http"
	"user_management/models"
	"user_management/services"
)

type userManagementHandler struct {
	userService services.UserManagementService
}

type Response struct {
	Status  int         `json:"status"`
	Message string      `json:"message"`
	Data    interface{} `json:"data"`
}

func NewUserManagementHandler(userService services.UserManagementService) userManagementHandler {
	return userManagementHandler{userService: userService}
}

func (h userManagementHandler) GetUserProfileByToken(c echo.Context) error {
	user := c.Get("user").(*jwt.Token)
	claims := user.Claims.(*models.AuthCustomClaims)

	response := new(Response)

	dataProfile, errGetProfile := h.userService.GetProfileUser(claims.AccountID)
	if errGetProfile != nil {
		response.Status = http.StatusInternalServerError
		response.Message = errGetProfile.Error()
		return c.JSON(http.StatusInternalServerError, response)
	}

	response.Status = http.StatusOK
	response.Message = "success"
	response.Data = dataProfile
	return c.JSON(http.StatusOK, response)
}

func (h userManagementHandler) GetOrderHistoryByToken(c echo.Context) error {
	response := new(Response)

	dataOrderHistory, errGetOrderHistory := h.userService.GetUserOrder(c.Request().Header.Get("Authorization"))
	if errGetOrderHistory != nil {
		response.Status = http.StatusInternalServerError
		response.Message = errGetOrderHistory.Error()
		return c.JSON(http.StatusInternalServerError, response)
	}

	response.Status = http.StatusOK
	response.Message = "success"
	response.Data = dataOrderHistory
	return c.JSON(http.StatusOK, response)
}
