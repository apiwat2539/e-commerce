package main

import (
	"fmt"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"user_management/database"
	"user_management/handler"
	"user_management/middlewares"
	"user_management/repository"
	"user_management/services"
)

func main() {
	fmt.Println("User Management Services")
	db, err := database.ConnectionDatabase()
	if err != nil {
		panic(err)
	}

	userRepo := repository.NewUserManagementRepository(db)
	userService := services.NewUserManagementService(userRepo)
	userHandler := handler.NewUserManagementHandler(userService)

	e := echo.New()

	// Middleware
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	e.Use(middlewares.CheckToken())

	e.GET("/profile", userHandler.GetUserProfileByToken)
	e.GET("/order_history", userHandler.GetOrderHistoryByToken)

	e.Logger.Fatal(e.Start(":1326"))
}
