package models

import "github.com/golang-jwt/jwt"

type AuthCustomClaims struct {
	AccountID int    `json:"account_id"`
	Username  string `json:"username"`
	jwt.StandardClaims
}
