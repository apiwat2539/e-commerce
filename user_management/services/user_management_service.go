package services

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"user_management/repository"
)

type userManagementService struct {
	userRepo repository.UserManagementRepository
}

func NewUserManagementService(userRepo repository.UserManagementRepository) UserManagementService {
	return userManagementService{userRepo: userRepo}
}

func (u userManagementService) GetProfileUser(userID int) (ResponseProfile, error) {
	//State : Get Profile User By User ID
	dataUser, err := u.userRepo.GetUserByID(userID)
	if err != nil {
		log.Println("GetProfileUser Error := errGetUserByID", err.Error())
		return ResponseProfile{}, err
	}

	response := ResponseProfile{
		Username:  dataUser.Username,
		FirstName: dataUser.FirstName,
		LastName:  dataUser.LastName,
		Address:   dataUser.Username,
		Status:    dataUser.Status,
		CreatedAt: dataUser.CreatedAt,
	}

	return response, nil
}

func (u userManagementService) GetUserOrder(token string) ([]ResponseOrderHistory, error) {
	//State : Query Order
	url := "http://localhost:1324/orders"

	response, err := callUrl(url, token)
	if err != nil {
		log.Println("GetUserOrder Error := errCallUrl", err)
		return nil, err
	}

	var responseApi ResponseApiGetOrderHistory
	var responseOrder []ResponseOrderHistory

	if errUnmarshal := json.Unmarshal(response, &responseApi); errUnmarshal != nil {
		log.Println("GetUserOrder Error := errUnmarshal", errUnmarshal)
		return nil, errUnmarshal
	}

	responseOrder = responseApi.Data
	return responseOrder, nil
}

func callUrl(url, token string) ([]byte, error) {
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}
	req.Header.Set("Authorization", token)

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	body, _ := ioutil.ReadAll(resp.Body)
	return body, nil
}
