package services

import "time"

type ResponseProfile struct {
	Username  string     `json:"username"`
	FirstName string     `json:"first_name"`
	LastName  string     `json:"last_name"`
	Address   string     `json:"address"`
	Status    string     `json:"status"`
	CreatedAt *time.Time `json:"created_at"`
}

type ResponseApiGetOrderHistory struct {
	Status  int                    `json:"status"`
	Message string                 `json:"message"`
	Data    []ResponseOrderHistory `json:"data"`
}

type ResponseOrderHistory struct {
	OrderID    int            `json:"order_id"`
	Products   []ListProducts `json:"products"`
	TotalPrice float64        `json:"total_price"`
}

type ListProducts struct {
	ProductID     int     `json:"product_id"`
	ProductName   string  `json:"product_name"`
	PathImage     string  `json:"path_image"`
	Price         float64 `json:"price"`
	ProductAmount int     `json:"product_amount"`
}

type UserManagementService interface {
	GetProfileUser(userID int) (ResponseProfile, error)
	GetUserOrder(token string) ([]ResponseOrderHistory, error)
}
