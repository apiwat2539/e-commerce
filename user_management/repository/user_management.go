package repository

import "time"

type Users struct {
	ID        int        `gorm:"column:id"`
	Username  string     `gorm:"column:username"`
	Password  string     `gorm:"column:password"`
	FirstName string     `gorm:"column:first_name"`
	LastName  string     `gorm:"column:last_name"`
	Address   string     `gorm:"column:address"`
	Status    string     `gorm:"column:status"`
	CreatedAt *time.Time `gorm:"column:created_at"`
	CreatedBy string     `gorm:"column:created_by"`
	UpdatedAt *time.Time `gorm:"column:updated_at"`
	UpdatedBy string     `gorm:"column:updated_by"`
	DeletedAt *time.Time `gorm:"column:deleted_at"`
	DeletedBy string     `gorm:"column:deleted_by"`
}

type Orders struct {
	ID         int        `gorm:"column:id"`
	UsersID    int        `gorm:"column:users_id"`
	TotalPrice float64    `gorm:"column:total_price"`
	Status     string     `gorm:"column:status"`
	SlipPath   string     `gorm:"column:slip_path"`
	Approved   string     `gorm:"column:approved"`
	CreatedAt  *time.Time `gorm:"column:created_at"`
	CreatedBy  string     `gorm:"column:created_by"`
	UpdatedAt  *time.Time `gorm:"column:updated_at"`
	UpdatedBy  string     `gorm:"column:updated_by"`
	DeletedAt  *time.Time `gorm:"column:deleted_at"`
	DeletedBy  string     `gorm:"column:deleted_by"`
}

type UserManagementRepository interface {
	GetUserByID(id int) (Users, error)
	GetListOrderByUserID(userID int) ([]Orders, error)
}
