package repository

import "gorm.io/gorm"

type userManagementDB struct {
	db *gorm.DB
}

func NewUserManagementRepository(db *gorm.DB) UserManagementRepository {
	return userManagementDB{db: db}
}

func (u userManagementDB) GetUserByID(id int) (Users, error) {
	user := new(Users)
	if errQueryUserByID := u.db.Where("id = ?", id).First(user).Error; errQueryUserByID != nil {
		return Users{}, errQueryUserByID
	}
	return *user, nil
}

func (u userManagementDB) GetListOrderByUserID(userID int) ([]Orders, error) {
	orders := new([]Orders)

	if errGetOrderByUserID := u.db.Where("users_id = ?", userID).Find(orders).Error; errGetOrderByUserID != nil {
		return *orders, errGetOrderByUserID
	}

	return *orders, nil
}
