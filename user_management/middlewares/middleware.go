package middlewares

import (
	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"user_management/models"
)

func CheckToken() echo.MiddlewareFunc {
	key := []byte(`-----BEGIN PUBLIC KEY-----
MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCK1HSpYE44VJHdR26Ha73ih7Mb
LPYJu5+OnBledseWIWNexhLi4zzwntM3ptRuF1mjpYZllrKj+9NQPJQAbujWadjp
Rd7VZOg5vMncRpZsGvKbHJpmlyZhXwGE3HsNUzuUoKo9dpjpxCkf4uTVaDFK8fLM
GIBY06UgvzWUFn2N4QIDAQAB
-----END PUBLIC KEY-----`)
	publicKey, _ := jwt.ParseRSAPublicKeyFromPEM(key)
	return middleware.JWTWithConfig(middleware.JWTConfig{
		TokenLookup:   "header:" + echo.HeaderAuthorization,
		SigningMethod: "RS256",
		AuthScheme:    "Bearer",
		Claims:        &models.AuthCustomClaims{},
		SigningKey:    publicKey,
	})
}
