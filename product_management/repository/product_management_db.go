package repository

import "gorm.io/gorm"

type productManagementRepository struct {
	db *gorm.DB
}

func NewProductManagementRepository(db *gorm.DB) ProductManagementRepository {
	return productManagementRepository{db: db}
}

func (p productManagementRepository) GetAllProduct() ([]Products, error) {
	listProduct := new([]Products)

	if errGetAllProduct := p.db.Find(listProduct).Error; errGetAllProduct != nil {
		return nil, errGetAllProduct
	}

	return *listProduct, nil
}

func (p productManagementRepository) GetProductByID(id int) (Products, error) {
	product := new(Products)

	if errGetAllProduct := p.db.Where("id = ?", id).First(product).Error; errGetAllProduct != nil {
		return Products{}, errGetAllProduct
	}

	return *product, nil
}
