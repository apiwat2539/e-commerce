package repository

import "time"

type Products struct {
	ID            int        `gorm:"column:id"`
	ProductName   string     `gorm:"column:product_name"`
	PathImage     string     `gorm:"column:path_image"`
	ProductAmount int        `gorm:"column:product_amount"`
	Status        string     `gorm:"column:status"`
	Price         float64    `gorm:"column:price"`
	CreatedAt     *time.Time `gorm:"column:created_at"`
	CreatedBy     string     `gorm:"column:created_by"`
	UpdatedAt     *time.Time `gorm:"column:updated_at"`
	UpdatedBy     string     `gorm:"column:updated_by"`
	DeletedAt     *time.Time `gorm:"column:deleted_at"`
	DeletedBy     string     `gorm:"column:deleted_by"`
}

type ProductManagementRepository interface {
	GetAllProduct() ([]Products, error)
	GetProductByID(id int) (Products, error)
}
