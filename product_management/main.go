package main

import (
	"fmt"
	"github.com/go-playground/validator"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"net/http"
	"product_management/database"
	"product_management/handler"
	"product_management/middlewares"
	"product_management/repository"
	"product_management/services"
)

func main() {
	fmt.Println("Product Management Services")
	db, err := database.ConnectionDatabase()
	if err != nil {
		panic(err)
	}

	productRepo := repository.NewProductManagementRepository(db)
	productService := services.NewProductManagementService(productRepo)
	productHandler := handler.NewProductManagementHandler(productService)

	e := echo.New()

	// Middleware
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())
	e.Use(middlewares.CheckToken())

	e.Validator = &CustomValidator{validator: validator.New()}

	e.GET("/products", productHandler.GetAllProducts)
	e.GET("/product/:id", productHandler.GetProductByID)

	e.Logger.Fatal(e.Start(":1325"))
}

type CustomValidator struct {
	validator *validator.Validate
}

func (cv *CustomValidator) Validate(i interface{}) error {
	if err := cv.validator.Struct(i); err != nil {
		// Optionally, you could return the error to give each route more control over the status code
		return echo.NewHTTPError(http.StatusBadRequest, err.Error())
	}
	return nil
}
