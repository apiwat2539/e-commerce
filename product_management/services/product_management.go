package services

type ResponseProduct struct {
	ID            int     `json:"id"`
	ProductName   string  `json:"product_name"`
	PathImage     string  `json:"path_image"`
	ProductAmount int     `json:"product_amount"`
	Status        string  `json:"status"`
	Price         float64 `json:"price"`
}

type ProductManagementService interface {
	GetAllProducts() ([]ResponseProduct, error)
	GetDetailProduct(id int) (ResponseProduct, error)
}
