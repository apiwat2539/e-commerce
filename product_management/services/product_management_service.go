package services

import (
	"log"
	"product_management/repository"
)

type productManagementService struct {
	productRepo repository.ProductManagementRepository
}

func (p productManagementService) GetAllProducts() ([]ResponseProduct, error) {
	productResponse := new([]ResponseProduct)

	listProduct, errGetListProduct := p.productRepo.GetAllProduct()
	if errGetListProduct != nil {
		log.Println("GetAllProducts Error := errGetListProduct", errGetListProduct)
		return nil, errGetListProduct
	}

	//State : loop append product
	for _, value := range listProduct {
		product := ResponseProduct{
			ID:            value.ID,
			ProductName:   value.ProductName,
			ProductAmount: value.ProductAmount,
			Status:        value.Status,
			PathImage:     value.PathImage,
			Price:         value.Price,
		}

		*productResponse = append(*productResponse, product)
	}

	return *productResponse, nil
}

func (p productManagementService) GetDetailProduct(id int) (ResponseProduct, error) {
	product, errGetProduct := p.productRepo.GetProductByID(id)
	if errGetProduct != nil {
		log.Println("GetDetailProduct Error := errGetProduct", errGetProduct)
		return ResponseProduct{}, errGetProduct
	}

	responseProduct := ResponseProduct{
		ID:            product.ID,
		ProductName:   product.ProductName,
		ProductAmount: product.ProductAmount,
		Status:        product.Status,
		PathImage:     product.PathImage,
		Price:         product.Price,
	}

	return responseProduct, nil
}

func NewProductManagementService(productRepo repository.ProductManagementRepository) ProductManagementService {
	return productManagementService{productRepo: productRepo}
}
