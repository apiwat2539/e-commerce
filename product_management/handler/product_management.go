package handler

import (
	"github.com/golang-jwt/jwt"
	"github.com/labstack/echo/v4"
	"log"
	"net/http"
	"product_management/services"
	"strconv"
)

type authCustomClaims struct {
	AccountID int    `json:"account_id"`
	Username  string `json:"username"`
	jwt.StandardClaims
}

type Response struct {
	Status  int         `json:"status"`
	Message string      `json:"message"`
	Data    interface{} `json:"data"`
}

type productManagementHandler struct {
	productManagementService services.ProductManagementService
}

func NewProductManagementHandler(productManagementService services.ProductManagementService) productManagementHandler {
	return productManagementHandler{productManagementService: productManagementService}
}

func (h productManagementHandler) GetAllProducts(c echo.Context) error {
	response := new(Response)

	listProducts, errGetListProducts := h.productManagementService.GetAllProducts()
	if errGetListProducts != nil {
		log.Println("GetAllProducts Error := errGetListProducts", errGetListProducts.Error())
		response.Status = http.StatusInternalServerError
		response.Message = errGetListProducts.Error()
		return c.JSON(http.StatusInternalServerError, response)
	}

	response.Status = http.StatusOK
	response.Message = "success"
	response.Data = listProducts
	return c.JSON(http.StatusOK, response)
}

func (h productManagementHandler) GetProductByID(c echo.Context) error {
	response := new(Response)

	req := c.Param("id")

	//State : convert req to int
	productID, errConvert := strconv.Atoi(req)
	if errConvert != nil {
		log.Println("GetAllProducts Error := errConvert", errConvert.Error())
		response.Status = http.StatusBadRequest
		response.Message = errConvert.Error()
		return c.JSON(http.StatusBadRequest, response)
	}

	product, errGetProducts := h.productManagementService.GetDetailProduct(productID)
	if errGetProducts != nil {
		log.Println("GetAllProducts Error := errGetProducts", errGetProducts.Error())
		response.Status = http.StatusInternalServerError
		response.Message = errGetProducts.Error()
		return c.JSON(http.StatusInternalServerError, response)
	}

	response.Status = http.StatusOK
	response.Message = "success"
	response.Data = product
	return c.JSON(http.StatusOK, response)
}
