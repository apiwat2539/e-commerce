package main

import (
	"fmt"
	"github.com/go-playground/validator"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"net/http"
	"order_management/database"
	"order_management/handler"
	"order_management/middlewares"
	"order_management/repository"
	"order_management/services"
)

func main() {
	fmt.Println("Order Management Services")
	db, err := database.ConnectionDatabase()
	if err != nil {
		panic(err)
	}

	orderManagementDB := repository.NewOrderManagementDB(db)
	billRepo := repository.NewBillManagementDB(db)
	orderManagementService := services.NewOrderManagementService(orderManagementDB, billRepo)
	orderManagementHandler := handler.NewOrderManagementHandler(orderManagementService)

	e := echo.New()

	// Middleware
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	e.Validator = &CustomValidator{validator: validator.New()}

	// Configure middlewares with the custom claims type
	e.Use(middlewares.CheckToken())
	e.POST("/order", orderManagementHandler.CreateNewOrder)
	e.GET("/orders", orderManagementHandler.GetOrderHistory)
	e.GET("/order/:id", orderManagementHandler.GetDetailOrder)
	e.PUT("/order", orderManagementHandler.CancelOrder)

	e.Logger.Fatal(e.Start(":1324"))
}

type CustomValidator struct {
	validator *validator.Validate
}

func (cv *CustomValidator) Validate(i interface{}) error {
	if err := cv.validator.Struct(i); err != nil {
		// Optionally, you could return the error to give each route more control over the status code
		return echo.NewHTTPError(http.StatusBadRequest, err.Error())
	}
	return nil
}
