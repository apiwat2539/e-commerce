package repository

import "gorm.io/gorm"

type orderManagementDB struct {
	db *gorm.DB
}

func NewOrderManagementDB(db *gorm.DB) OrderManagementRepository {
	return &orderManagementDB{db: db}
}

func (o orderManagementDB) CreateNewOrder(order *Orders) error {
	if errCreateNewOrder := o.db.Create(order).Error; errCreateNewOrder != nil {
		return errCreateNewOrder
	}
	return nil
}

func (o orderManagementDB) GetOrderByOrderID(id int) (*Orders, error) {
	orders := new(Orders)

	if errGetOrderByOrderID := o.db.Where("id = ?", id).First(orders).Error; errGetOrderByOrderID != nil {
		return orders, errGetOrderByOrderID
	}
	return orders, nil
}

func (o orderManagementDB) UpdateOrderByID(order Orders) error {
	if errUpdateStatusCancelOrder := o.db.Model(Orders{}).Where("id = ?", order.ID).Updates(order).Error; errUpdateStatusCancelOrder != nil {
		return errUpdateStatusCancelOrder
	}
	return nil
}

func (o orderManagementDB) GetAllOrderByUserID(userID int) ([]Orders, error) {
	orders := new([]Orders)

	if errGetOrderByUserID := o.db.Where("users_id = ?", userID).Find(orders).Error; errGetOrderByUserID != nil {
		return *orders, errGetOrderByUserID
	}
	return *orders, nil
}

func (o orderManagementDB) CreateBills(dataBill Bills) error {
	if errCreateBill := o.db.Create(&dataBill).Error; errCreateBill != nil {
		return errCreateBill
	}
	return nil
}

func (o orderManagementDB) GetBillByOrderID(orderID int) (Bills, error) {
	dataBill := new(Bills)
	if errGetDataBill := o.db.Where("orders_id = ?", orderID).First(dataBill).Error; errGetDataBill != nil {
		return *dataBill, errGetDataBill
	}
	return *dataBill, nil
}
