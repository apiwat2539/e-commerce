package repository

import "time"

type Orders struct {
	ID         int        `gorm:"column:id"`
	UsersID    int        `gorm:"column:users_id"`
	TotalPrice float64    `gorm:"column:total_price"`
	Status     string     `gorm:"column:status"`
	SlipPath   string     `gorm:"column:slip_path"`
	Approved   string     `gorm:"column:approved"`
	CreatedAt  *time.Time `gorm:"column:created_at"`
	CreatedBy  string     `gorm:"column:created_by"`
	UpdatedAt  *time.Time `gorm:"column:updated_at"`
	UpdatedBy  string     `gorm:"column:updated_by"`
	DeletedAt  *time.Time `gorm:"column:deleted_at"`
	DeletedBy  string     `gorm:"column:deleted_by"`
}

type ResponseGetProducts struct {
	Status  int    `json:"status"`
	Message string `json:"message"`
	Data    []struct {
		ID            int     `json:"id"`
		ProductName   string  `json:"product_name"`
		Price         float64 `json:"price"`
		PathImage     string  `json:"path_image"`
		ProductAmount int     `json:"product_amount"`
	} `json:"data"`
}

type OrderManagementRepository interface {
	CreateNewOrder(order *Orders) error
	GetOrderByOrderID(id int) (*Orders, error)
	GetAllOrderByUserID(userID int) ([]Orders, error)
	UpdateOrderByID(order Orders) error
	GetBillByOrderID(orderID int) (Bills, error)
}
