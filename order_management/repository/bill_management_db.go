package repository

import (
	"gorm.io/gorm"
)

type billManagementDB struct {
	db *gorm.DB
}

func NewBillManagementDB(db *gorm.DB) BillManagementRepository {
	return billManagementDB{db: db}
}

func (b billManagementDB) GetBillByOrderID(orderID int) ([]Bills, error) {
	dataBill := new([]Bills)

	if errQueryBillByOrderID := b.db.Where("orders_id = ?", orderID).Find(dataBill).Error; errQueryBillByOrderID != nil {
		return nil, errQueryBillByOrderID
	}

	return *dataBill, nil
}

func (b billManagementDB) CreateBill(dataBill Bills) error {
	if errCreateBill := b.db.Create(&dataBill).Error; errCreateBill != nil {
		return errCreateBill
	}
	return nil
}

func (b billManagementDB) GetProductByID(id int) (Products, error) {
	product := new(Products)

	if errGetAllProduct := b.db.Where("id = ?", id).First(product).Error; errGetAllProduct != nil {
		return Products{}, errGetAllProduct
	}

	return *product, nil
}
