package handler

import (
	"github.com/golang-jwt/jwt"
	"github.com/labstack/echo/v4"
	"log"
	"net/http"
	"order_management/models"
	"order_management/services"
	"strconv"
)

type orderManagementHandler struct {
	orderManagementService services.OrderManagementService
}

type Response struct {
	Status  int         `json:"status"`
	Message string      `json:"message"`
	Data    interface{} `json:"data"`
}

func NewOrderManagementHandler(orderManagementService services.OrderManagementService) orderManagementHandler {
	return orderManagementHandler{orderManagementService: orderManagementService}
}

func (h orderManagementHandler) GetOrderHistory(c echo.Context) error {
	user := c.Get("user").(*jwt.Token)
	claims := user.Claims.(*models.AuthCustomClaims)

	response := new(Response)

	listOrder, errGetListOrder := h.orderManagementService.GetListOrderHistory(claims.AccountID, c.Request().Header.Get("Authorization"))
	if errGetListOrder != nil {
		log.Println("GetOrderHistory Error := errGetListOrder", errGetListOrder.Error())
		response.Status = http.StatusInternalServerError
		response.Message = errGetListOrder.Error()
		return c.JSON(http.StatusInternalServerError, response)
	}

	response.Status = http.StatusOK
	response.Message = "success"
	response.Data = listOrder
	return c.JSON(http.StatusOK, response)
}

func (h orderManagementHandler) GetDetailOrder(c echo.Context) error {
	response := new(Response)
	reqOrderID := c.Param("id")

	//State : Convert Order ID
	orderID, err := strconv.Atoi(reqOrderID)
	if err != nil {
		log.Println("GetDetailOrder := error ", err)
		response.Status = http.StatusBadRequest
		response.Message = err.Error()
		return c.JSON(http.StatusBadRequest, response)
	}

	orderHistory, errGetDetailOrder := h.orderManagementService.GetDetailOrderHistory(orderID)
	if errGetDetailOrder != nil {
		log.Println("GetDetailOrder Error := errGetDetailOrder", errGetDetailOrder)
		response.Status = http.StatusInternalServerError
		response.Message = errGetDetailOrder.Error()
		return c.JSON(http.StatusInternalServerError, response)
	}

	response.Status = http.StatusOK
	response.Message = "success"
	response.Data = orderHistory
	return c.JSON(http.StatusOK, response)
}

func (h orderManagementHandler) CreateNewOrder(c echo.Context) error {
	user := c.Get("user").(*jwt.Token)
	claims := user.Claims.(*models.AuthCustomClaims)

	response := new(Response)
	req := new(services.RequestCreateNewOrder)

	if errBindRequest := c.Bind(req); errBindRequest != nil {
		log.Println("CreateNewOrder Error := errBindRequest", errBindRequest)
		response.Status = http.StatusBadRequest
		response.Message = errBindRequest.Error()
		return c.JSON(http.StatusBadRequest, response)
	}

	req.UserID = claims.AccountID
	if errCreateNewOrder := h.orderManagementService.CreateNewOrder(*req, c.Request().Header.Get("Authorization")); errCreateNewOrder != nil {
		log.Println("CreateNewOrder Error := errCreateNewOrder", errCreateNewOrder)
		response.Status = http.StatusInternalServerError
		response.Message = errCreateNewOrder.Error()
		return c.JSON(http.StatusInternalServerError, response)
	}

	response.Status = http.StatusOK
	response.Message = "success"
	return c.JSON(http.StatusOK, response)
}

func (h orderManagementHandler) CancelOrder(c echo.Context) error {
	response := new(Response)
	req := new(services.RequestCancelOrder)

	if errBindRequest := c.Bind(req); errBindRequest != nil {
		log.Println("CancelOrder Error := errBindRequest", errBindRequest)
		response.Status = http.StatusBadRequest
		response.Message = errBindRequest.Error()
		return c.JSON(http.StatusBadRequest, response)
	}

	if errValidate := c.Validate(req); errValidate != nil {
		log.Println("CancelOrder Error:= errValidate", errValidate.Error())
		response.Status = http.StatusBadRequest
		response.Message = errValidate.Error()
		return c.JSON(http.StatusBadRequest, response)
	}

	if errCancelOrder := h.orderManagementService.CancelOrder(req.ID); errCancelOrder != nil {
		log.Println("CancelOrder Error := errCancelOrder", errCancelOrder)
		response.Status = http.StatusInternalServerError
		response.Message = errCancelOrder.Error()
		return c.JSON(http.StatusInternalServerError, response)
	}

	response.Status = http.StatusOK
	response.Message = "success"
	return c.JSON(http.StatusOK, response)
}
