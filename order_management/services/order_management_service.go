package services

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"order_management/repository"
	"sync"
	"time"
)

type orderManagementService struct {
	orderRepo repository.OrderManagementRepository
	billRepo  repository.BillManagementRepository
}

func NewOrderManagementService(orderRepo repository.OrderManagementRepository, billRepo repository.BillManagementRepository) OrderManagementService {
	return orderManagementService{orderRepo: orderRepo, billRepo: billRepo}
}

func (o orderManagementService) GetDetailOrderHistory(id int) (ResponseOrderHistory, error) {
	listProducts := new([]ListProducts)

	//State : Get Order History
	order, err := o.orderRepo.GetOrderByOrderID(id)
	if err != nil {
		log.Println("GetDetailOrderHistory GetOrderByOrderID := error", err)
		return ResponseOrderHistory{}, err
	}

	//State : Get Bill
	dataBill, err := o.billRepo.GetBillByOrderID(order.ID)
	if err != nil {
		log.Println("GetDetailOrderHistory GetBillByOrderID := error", err)
		return ResponseOrderHistory{}, err
	}

	//State : Loop Set List Products
	for _, value := range dataBill {
		dataProduct, err := o.billRepo.GetProductByID(value.ProductsID)
		if err != nil {
			log.Println("GetDetailOrderHistory GetProductByID := error", err)
			break
		}

		product := ListProducts{
			ProductID:     dataProduct.ID,
			ProductName:   dataProduct.ProductName,
			ProductAmount: dataProduct.ProductAmount,
			Price:         dataProduct.Price,
			PathImage:     dataProduct.PathImage,
		}
		*listProducts = append(*listProducts, product)
	}

	dataOrder := ResponseOrderHistory{
		OrderID:    order.ID,
		TotalPrice: order.TotalPrice,
		Products:   *listProducts,
	}
	return dataOrder, nil
}

func (o orderManagementService) GetListOrderHistory(userID int, token string) ([]ResponseOrderHistory, error) {
	listOrder := new([]ResponseOrderHistory)
	responseApiGetProduct := new(repository.ResponseGetProducts)

	var wg sync.WaitGroup
	wg.Add(1)
	go func() {
		defer wg.Done()
		if err := getProductListFromMicroserviceProducts(responseApiGetProduct, token); err != nil {
			log.Println("GetListOrderHistory Error := errGetProductListFromMicroserviceProducts", err.Error())
		}
	}()

	//State : Get Order History
	order, err := o.orderRepo.GetAllOrderByUserID(userID)
	if err != nil {
		log.Println("GetDetailOrderHistory GetOrderByOrderID := error", err)
		return nil, err
	}

	wg.Wait()
	for _, valueOrder := range order {
		listProducts := new([]ListProducts)

		//State : Get Bill
		dataBill, errGetDataBill := o.billRepo.GetBillByOrderID(valueOrder.ID)
		if errGetDataBill != nil {
			log.Println("GetDetailOrderHistory GetOrderByOrderID := error", err)
			return nil, errGetDataBill
		}

		//State : Loop Set List Products
		for _, bill := range dataBill {
			for _, product := range responseApiGetProduct.Data {
				if bill.ProductsID == product.ID {
					dataProduct := ListProducts{
						ProductID:     product.ID,
						ProductName:   product.ProductName,
						ProductAmount: product.ProductAmount,
						Price:         product.Price,
						PathImage:     product.PathImage,
					}

					*listProducts = append(*listProducts, dataProduct)
				}
			}

		}

		dataOrder := ResponseOrderHistory{
			OrderID:    valueOrder.ID,
			TotalPrice: valueOrder.TotalPrice,
			Products:   *listProducts,
			Status:     valueOrder.Status,
			SlipPath:   valueOrder.SlipPath,
		}
		*listOrder = append(*listOrder, dataOrder)
	}

	return *listOrder, nil
}

func (o orderManagementService) CreateNewOrder(req RequestCreateNewOrder, token string) error {
	responseApiGetProduct := new(repository.ResponseGetProducts)

	var wg sync.WaitGroup
	wg.Add(1)
	go func() {
		defer wg.Done()
		if err := getProductListFromMicroserviceProducts(responseApiGetProduct, token); err != nil {
			log.Println("CreateNewOrder Error := errGetProductListFromMicroserviceProducts", err.Error())
		}
	}()

	order := repository.Orders{
		UsersID: req.UserID,
		Status:  "waiting",
	}

	if errInsertOrder := o.orderRepo.CreateNewOrder(&order); errInsertOrder != nil {
		log.Println("CreateNewOrder CreateNewOrder := error ", errInsertOrder)
		return errInsertOrder
	}

	wg.Wait()
	//State : Loop For Insert Bill
	for _, reqProductID := range req.ListProductID {
		for _, value := range responseApiGetProduct.Data {
			if reqProductID == value.ID {
				dataBill := repository.Bills{
					OrdersID:   order.ID,
					ProductsID: value.ID,
					BillNo:     fmt.Sprintf("bill-%d", time.Now().Unix()),
				}

				//State : Create Bill
				if errCreateBill := o.billRepo.CreateBill(dataBill); errCreateBill != nil {
					log.Println("CreateNewOrder CreateBills := error ", errCreateBill)
					return errCreateBill
				}
			}
		}
	}

	return nil
}

func (o orderManagementService) CancelOrder(id int) error {
	//State : Get Order
	dataOrder, errGetOrder := o.orderRepo.GetOrderByOrderID(id)
	if errGetOrder != nil {
		log.Println("CancelOrder GetOrderByOrderID := error ", errGetOrder)
		return errGetOrder
	}

	//State : Set Status
	dataOrder.Status = "cancel"

	//State : Update Order
	if errUpdateDataOrder := o.orderRepo.UpdateOrderByID(*dataOrder); errUpdateDataOrder != nil {
		log.Println("CancelOrder errUpdateDataOrder := error ", errGetOrder)
		return errGetOrder
	}

	return nil
}

func callUrl(url, token string) ([]byte, error) {
	fmt.Println("callUrl")
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}
	req.Header.Set("Authorization", token)

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	body, _ := ioutil.ReadAll(resp.Body)
	return body, nil
}

func getProductListFromMicroserviceProducts(responseFromApi *repository.ResponseGetProducts, token string) error {
	strUrl := fmt.Sprintf("http://localhost:1325/products")

	responseByte, err := callUrl(strUrl, token)
	if err != nil {
		log.Println("getProductListFromMicroserviceProducts callUrl := error ", err)
		return err
	}

	if errUnmarshal := json.Unmarshal(responseByte, responseFromApi); errUnmarshal != nil {
		log.Println("getProductListFromMicroserviceProducts json.Unmarshal := error ", errUnmarshal)
		return errUnmarshal
	}

	return nil
}
