package services

type ResponseOrderHistory struct {
	OrderID    int            `json:"order_id"`
	Products   []ListProducts `json:"products"`
	TotalPrice float64        `json:"total_price"`
	Status     string         `json:"status"`
	SlipPath   string         `json:"slip_path"`
}

type ListProducts struct {
	ProductID     int     `json:"product_id"`
	ProductName   string  `json:"product_name"`
	PathImage     string  `json:"path_image"`
	Price         float64 `json:"price"`
	ProductAmount int     `json:"product_amount"`
}

type RequestCreateNewOrder struct {
	UserID        int   `json:"user_id"`
	ListProductID []int `json:"list_product_id" validate:"len>0"`
}

type RequestCancelOrder struct {
	ID int `json:"id" validate:"required"`
}

type OrderManagementService interface {
	GetDetailOrderHistory(id int) (ResponseOrderHistory, error)
	GetListOrderHistory(userID int, token string) ([]ResponseOrderHistory, error)
	CreateNewOrder(req RequestCreateNewOrder, token string) error
	CancelOrder(id int) error
}
