package main

import (
	"authentication/database"
	"authentication/handler"
	"authentication/repository"
	"authentication/services"
	"fmt"
	"github.com/go-playground/validator"
	"github.com/labstack/echo/v4"
	"net/http"
)

func main() {
	fmt.Println("Authentication Services")
	db, err := database.ConnectionDatabase()
	if err != nil {
		panic(err)
	}

	authDB := repository.NewAuthenticationDB(db)
	authService := services.NewAuthenticationService(authDB)
	authHandler := handler.NewAuthenticationHandler(authService)

	e := echo.New()
	e.Validator = &CustomValidator{validator: validator.New()}

	e.POST("/login", authHandler.Login)
	e.POST("/register", authHandler.Registration)

	e.Logger.Fatal(e.Start(":1323"))
}

type CustomValidator struct {
	validator *validator.Validate
}

func (cv *CustomValidator) Validate(i interface{}) error {
	if err := cv.validator.Struct(i); err != nil {
		// Optionally, you could return the error to give each route more control over the status code
		return echo.NewHTTPError(http.StatusBadRequest, err.Error())
	}
	return nil
}
