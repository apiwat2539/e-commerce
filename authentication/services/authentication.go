package services

import "github.com/golang-jwt/jwt"

type RequestLogin struct {
	Username string `json:"username" validate:"required"`
	Password string `json:"password" validate:"required"`
}

type RequestRegistration struct {
	Username  string `json:"username" validate:"required"`
	Password  string `json:"password" validate:"required"`
	Firstname string `json:"firstname" validate:"required"`
	Lastname  string `json:"lastname" validate:"required"`
	Address   string `json:"address" validate:"required"`
}

type UserResponse struct {
	Username  string `json:"username"`
	Firstname string `json:"firstname"`
	Lastname  string `json:"lastname"`
	Address   string `json:"address"`
}

type authCustomClaims struct {
	AccountID int    `json:"account_id"`
	Username  string `json:"username"`
	jwt.StandardClaims
}

type AuthenticationService interface {
	Registration(req *RequestRegistration) (UserResponse, error)
	UserLogin(req *RequestLogin) (string, error)
}
