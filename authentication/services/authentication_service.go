package services

import (
	"authentication/repository"
	"github.com/golang-jwt/jwt"
	"golang.org/x/crypto/bcrypt"
	"log"
	"time"
)

type authenticationService struct {
	authRepo repository.AuthenticationRepository
}

func NewAuthenticationService(authRepo repository.AuthenticationRepository) authenticationService {
	return authenticationService{authRepo: authRepo}
}

func (s authenticationService) Registration(req *RequestRegistration) (UserResponse, error) {
	data, err := s.authRepo.CreateNewUser(repository.Users{
		Username:  req.Username,
		Password:  hashPassword(req.Password),
		Firstname: req.Firstname,
		Lastname:  req.Lastname,
		Address:   req.Address,
	})

	if err != nil {
		log.Println("authenticationService Registration := error ", err)
		return UserResponse{}, err
	}

	dataUser := UserResponse{
		Username:  data.Username,
		Firstname: data.Firstname,
		Lastname:  data.Lastname,
		Address:   data.Address,
	}

	return dataUser, nil
}

func (s authenticationService) UserLogin(req *RequestLogin) (string, error) {
	var err error
	var message string

	//State : Insert Login Table
	defer func() {
		if err != nil {
			login := repository.Login{
				Username: req.Username,
				Status:   "500",
				Message:  message,
			}
			if errInsertLogLogin := s.authRepo.InsertLoginHistory(login); errInsertLogLogin != nil {
				log.Println("authenticationService UserLogin := error ", errInsertLogLogin)
			}
		} else {
			login := repository.Login{
				Username: req.Username,
				Status:   "200",
				Message:  "success",
			}
			if errInsertLogLogin := s.authRepo.InsertLoginHistory(login); errInsertLogLogin != nil {
				log.Println("authenticationService UserLogin := error ", errInsertLogLogin)
			}
		}
	}()

	//State : Get User By Username
	dataUser, err := s.authRepo.GetCustomerByUsername(req.Username)
	if err != nil {
		log.Println("authenticationService Login := error ", err)
		message = err.Error()
		return "", err
	}

	//State : Check Hash Password
	errCheckHashPassword := checkPasswordHash(req.Password, dataUser.Password)
	if errCheckHashPassword != nil {
		log.Println("authenticationService Login checkPasswordHash := error ", errCheckHashPassword)
		message = errCheckHashPassword.Error()
		return "", errCheckHashPassword
	}

	//State : Generate Token
	tokenString := generateToken(dataUser.ID, dataUser.Username)

	return tokenString, nil
}

func generateToken(accountID int, username string) string {
	claims := jwt.NewWithClaims(jwt.SigningMethodRS256, authCustomClaims{
		Username:  username,
		AccountID: accountID,
		StandardClaims: jwt.StandardClaims{
			IssuedAt:  time.Now().Unix(),
			ExpiresAt: time.Now().Add(1 * time.Hour).Unix(),
		},
	})

	key := []byte("-----BEGIN RSA PRIVATE KEY-----\nMIICXQIBAAKBgQCK1HSpYE44VJHdR26Ha73ih7MbLPYJu5+OnBledseWIWNexhLi\n4zzwntM3ptRuF1mjpYZllrKj+9NQPJQAbujWadjpRd7VZOg5vMncRpZsGvKbHJpm\nlyZhXwGE3HsNUzuUoKo9dpjpxCkf4uTVaDFK8fLMGIBY06UgvzWUFn2N4QIDAQAB\nAoGABHYTUaYQnWrKTSwQdzWVIoRt0v+84E8xL2oAvrJec4qbIN6ImNXojWrtKqqn\nxOnnLYUSLo2LLA3Zmoh30RO/U6hFdItdn5rnzBAIHR7nTj4gi2Q78fAUH2H8o8br\nZzEjGdRmWQFVdkdoXVjPnxJiOiZRweg6MmArUzCqcYFjOGkCQQDfAlQvjcIWEWPT\nkWkHwkjgYllRkuEFBnsDBC7GYZyBxhL9lxkzRHl9ntpXkRLhVFxV/TWasO1KIJow\nbwuF+Y+vAkEAn14kLpuX/AJ6hUdiGo1+tXvk1BrzNnaDpDEmAM4Pv+tMstsN72X9\na2pU9e/amDWaqwsykrSaqC/BosFoQDMPbwJBAKiv1UhHPWl96/nsAvuIfaGlIxTu\nOmGXvlLQxiTvTvhnAJRrx/ccs51PHK+iIBrruhCdQP/rFwK8WIWMabYEyScCQDKd\nJYSf6d8hsmPNzfx837cnx/8NKB0t+pUU/urWhu3+/vb21zAnBhstRBpvuM8Khzq3\nbbJmJZAwyqC0KDXrgYUCQQCHYkTHeLbd1UH6m5/LxyRLyBTDIHEETNw1y/6RJw/F\nMl8wSNedFCLjNm/mZhxt44K1P7Gs3Ktl50jeWTfoDDWv\n-----END RSA PRIVATE KEY-----")
	privatekey, _ := jwt.ParseRSAPrivateKeyFromPEM(key)
	tokenString, errSignedStringAccessToken := claims.SignedString(privatekey)
	if errSignedStringAccessToken != nil {
		panic(errSignedStringAccessToken)
	}
	return tokenString
}

func hashPassword(password string) string {
	bytes, _ := bcrypt.GenerateFromPassword([]byte(password), 14)
	return string(bytes)
}

func checkPasswordHash(password, hash string) error {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err
}
