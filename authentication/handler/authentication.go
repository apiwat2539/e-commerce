package handler

import (
	"authentication/services"
	"github.com/labstack/echo/v4"
	"log"
	"net/http"
)

type authenticationHandler struct {
	authService services.AuthenticationService
}

type Response struct {
	Status  int         `json:"status"`
	Message string      `json:"message"`
	Data    interface{} `json:"data"`
}

func NewAuthenticationHandler(authService services.AuthenticationService) authenticationHandler {
	return authenticationHandler{authService: authService}
}

func (h authenticationHandler) Registration(c echo.Context) error {
	response := new(Response)
	requestRegistration := new(services.RequestRegistration)
	//State : Bind Request
	if errBindRequest := c.Bind(requestRegistration); errBindRequest != nil {
		log.Println("Registration Error:= errBindRequest", errBindRequest.Error())
		response.Status = http.StatusBadRequest
		response.Message = errBindRequest.Error()
		return c.JSON(http.StatusBadRequest, response)
	}

	if errValidate := c.Validate(requestRegistration); errValidate != nil {
		log.Println("Registration Error:= errValidate", errValidate.Error())
		response.Status = http.StatusBadRequest
		response.Message = errValidate.Error()
		return c.JSON(http.StatusBadRequest, response)
	}

	dataUser, errRegistration := h.authService.Registration(requestRegistration)
	if errRegistration != nil {
		log.Println("Registration Error:= errRegistration", errRegistration.Error())
		response.Status = http.StatusInternalServerError
		response.Message = errRegistration.Error()
		return c.JSON(http.StatusInternalServerError, response)
	}

	response.Status = http.StatusOK
	response.Message = "success"
	response.Data = dataUser
	return c.JSON(http.StatusOK, response)
}

func (h authenticationHandler) Login(c echo.Context) error {
	response := new(Response)
	requestLogin := new(services.RequestLogin)

	//State : Bind Request
	if errBindRequest := c.Bind(requestLogin); errBindRequest != nil {
		log.Println("Login Error:= errBindRequest", errBindRequest.Error())
		response.Status = http.StatusInternalServerError
		response.Message = errBindRequest.Error()
		return c.JSON(http.StatusBadRequest, errBindRequest.Error())
	}

	if errValidate := c.Validate(requestLogin); errValidate != nil {
		log.Println("Login Error:= errValidate", errValidate.Error())
		response.Status = http.StatusBadRequest
		response.Message = errValidate.Error()
		return c.JSON(http.StatusBadRequest, response)
	}

	token, errLogin := h.authService.UserLogin(requestLogin)
	if errLogin != nil {
		log.Println("Login Error:= errLogin", errLogin.Error())
		response.Status = http.StatusInternalServerError
		response.Message = errLogin.Error()
		return c.JSON(http.StatusInternalServerError, response)
	}

	response.Status = http.StatusOK
	response.Message = "success"
	response.Data = token
	return c.JSON(http.StatusOK, response)
}
