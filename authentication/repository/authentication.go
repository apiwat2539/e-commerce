package repository

import "time"

type Users struct {
	ID        int        `gorm:"column:id"`
	Username  string     `gorm:"column:username"`
	Password  string     `gorm:"column:password"`
	Firstname string     `gorm:"column:first_name"`
	Lastname  string     `gorm:"column:last_name"`
	Address   string     `gorm:"column:address"`
	Status    string     `gorm:"column:status"`
	CreatedAt *time.Time `gorm:"column:created_at"`
	CreatedBy string     `gorm:"column:created_by"`
	UpdatedAt *time.Time `gorm:"column:updated_at"`
	UpdatedBy string     `gorm:"column:updated_by"`
	DeletedAt *time.Time `gorm:"column:deleted_at"`
	DeletedBy string     `gorm:"column:deleted_by"`
}

type Login struct {
	Username  string     `gorm:"column:username"`
	Ip        string     `gorm:"column:ip"`
	Status    string     `gorm:"column:status"`
	Message   string     `gorm:"column:message"`
	CreatedAt *time.Time `gorm:"column:created_at"`
	CreatedBy string     `gorm:"column:created_by"`
}

func (Login) TableName() string {
	return "login"
}

type AuthenticationRepository interface {
	CreateNewUser(reqUser Users) (Users, error)
	GetCustomerByUsername(username string) (user *Users, err error)
	InsertLoginHistory(req Login) error
}
