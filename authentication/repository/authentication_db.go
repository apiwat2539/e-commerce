package repository

import (
	"gorm.io/gorm"
)

type authenticationDB struct {
	db *gorm.DB
}

func NewAuthenticationDB(db *gorm.DB) AuthenticationRepository {
	return &authenticationDB{db: db}
}

func (auth *authenticationDB) CreateNewUser(reqUser Users) (Users, error) {
	dataUser := &Users{
		Username:  reqUser.Username,
		Password:  reqUser.Password,
		Firstname: reqUser.Firstname,
		Lastname:  reqUser.Lastname,
		Address:   reqUser.Address,
		CreatedBy: reqUser.Username,
	}

	if errInsertNewUser := auth.db.Create(dataUser).Error; errInsertNewUser != nil {
		return Users{}, errInsertNewUser
	}
	return *dataUser, nil
}

func (auth *authenticationDB) GetCustomerByUsername(username string) (user *Users, err error) {
	dataUser := new(Users)

	if errGetDataUser := auth.db.Where("username = ?", username).First(dataUser).Error; errGetDataUser != nil {
		return nil, errGetDataUser
	}

	return dataUser, err
}

func (auth *authenticationDB) InsertLoginHistory(req Login) error {
	dataLogin := &Login{
		Username:  req.Username,
		Ip:        req.Ip,
		Status:    req.Status,
		Message:   req.Message,
		CreatedBy: req.Username,
	}
	if errInsertLoginHistory := auth.db.Create(dataLogin).Error; errInsertLoginHistory != nil {
		return errInsertLoginHistory
	}
	return nil
}
